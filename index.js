// 1.

function addTwoNumbers(num1, num2) {
  let addNumbers = num1 + num2;
  console.log("Displayed sum of " + num1 + " and " + num2);
  console.log(addNumbers);
}

function subTwoNumbers(num1, num2) {
  let subNumbers = num1 - num2;
  console.log("Displayed difference of " + num1 + " and " + num2);
  console.log(subNumbers);
}

addTwoNumbers(5, 15);
subTwoNumbers(20, 5);

// 2.
let product;
let quotient;

function mulTwoNumbers(num1, num2) {
  product = num1 * num2;
  console.log("The product  of " + num1 + " and " + num2);
  return product;
}
mulTwoNumbers(50, 10);
console.log(product);

function divTwoNumbers(num1, num2) {
  quotient = num1 / num2;
  console.log("The product  of " + num1 + " and " + num2);
  return quotient;
}
divTwoNumbers(50, 10);
console.log(quotient);

// 3.
const PI = 3.14159;
function getTotalAreaCircle(radius) {
  let circleArea = PI * radius ** 2;
  console.log(
    "The result of getting the are of a circle with " + radius + " radius."
  );
  return circleArea;
}

let cirArea = console.log(getTotalAreaCircle(15));

// 4.

function getTotalAve(num1, num2, num3, num4) {
  let average = (num1 + num2 + num3 + num4) / 4;
  console.log(
    "The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4
  );
  return average;
}
let totalAverage = console.log(getTotalAve(20, 40, 60, 80));

// 5.

function checkIfPassed(score, totalScore) {
  let percentage = (score / totalScore) * 100;
  isPassed = percentage > 75;
  console.log("Is " + score + "/" + totalScore + " a passing score?");
  return isPassed;
}
let checkGrade = console.log(checkIfPassed(38, 50));
